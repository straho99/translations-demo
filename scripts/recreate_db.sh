#!/usr/bin/env bash
symfony php bin/console doctrine:database:drop --force
symfony php bin/console doctrine:database:create
symfony php bin/console doctrine:schema:update --force
symfony php bin/console doctrine:fixtures:load --no-interaction