<?php

namespace App\Repository;

use App\Entity\Translation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Translation>
 *
 * @method Translation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Translation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Translation[]    findAll()
 * @method Translation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Translation::class);
    }

    public function save(Translation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Translation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function filter(string $locale, string $domain, ?string $key = null, ?string $value = null): array
    {
        $builder = $this->createQueryBuilder('translation')
            ->orderBy('translation.translationKey', 'asc')
            ->andWhere('translation.locale = :locale')
            ->andWhere('translation.domain = :domain')
            ->setParameter('locale', $locale)
            ->setParameter('domain', $domain)
            ;

        if ($key !== null) {
            $builder
                ->andWhere('translation.translationKey like :key')
                ->setParameter('key', "%$key%");
        }

        if ($value !== null) {
            $builder
                ->andWhere('translation.translationValue like :value')
                ->setParameter('value', "%$value%");
        }

        return $builder->getQuery()->getResult();
    }
}
