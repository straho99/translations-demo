<?php

namespace App\Repository;

use App\Entity\FavoriteProduct;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function save(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function notAdded(): array
    {
        $favorites = $this->getEntityManager()->getRepository(FavoriteProduct::class)->findAll();
        $favorites = array_map(fn(FavoriteProduct $favoriteProduct) => $favoriteProduct->getProduct(), $favorites);
        $favorites = array_map(fn(Product $product) => $product->getId(), $favorites);

        if (empty($favorites)) {
            $favorites = [-1];
        }

        return $this->createQueryBuilder('product')
            ->andWhere('product.id not in (:favorites)')
            ->setParameter('favorites', $favorites)
            ->getQuery()
            ->getResult();
    }
}
