<?php

namespace App\DataFixtures;

use App\Factory\TranslationFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TranslationFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        TranslationFactory::createMany(50, [
            'locale' => 'en',
            'domain' => 'text',
        ]);
        TranslationFactory::createMany(50, [
            'locale' => 'bg',
            'domain' => 'text',
        ]);

        TranslationFactory::createMany(50, [
            'locale' => 'en',
            'domain' => 'validators',
        ]);
        TranslationFactory::createMany(50, [
            'locale' => 'bg',
            'domain' => 'validators',
        ]);

        TranslationFactory::createMany(50, [
            'locale' => 'en',
            'domain' => 'messages',
        ]);
        TranslationFactory::createMany(50, [
            'locale' => 'bg',
            'domain' => 'messages',
        ]);
    }
}
