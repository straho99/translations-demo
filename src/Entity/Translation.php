<?php

namespace App\Entity;

use App\Repository\TranslationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TranslationRepository::class)]
class Translation
{
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    #[ORM\Id]
    private ?int $id = null;

    #[ORM\Column(type: 'string')]
    private ?string $locale = null;

    #[ORM\Column(type: 'string')]
    private ?string $domain = null;

    #[ORM\Column(type: 'string')]
    private ?string $translationKey = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $translationValue = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getTranslationKey(): ?string
    {
        return $this->translationKey;
    }

    public function setTranslationKey(string $translationKey): self
    {
        $this->translationKey = $translationKey;

        return $this;
    }

    public function getTranslationValue(): ?string
    {
        return $this->translationValue;
    }

    public function setTranslationValue(?string $translationValue): self
    {
        $this->translationValue = $translationValue;

        return $this;
    }
}
