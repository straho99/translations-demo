<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FavoriteProductController extends BaseController
{
    #[Route(path: '', name: 'favorite_product.index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('product/index.html.twig');
    }
}
