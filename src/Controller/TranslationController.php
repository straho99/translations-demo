<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TranslationController extends BaseController
{
    #[Route(path: '/translations', name: 'translations.index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('translation/index.html.twig', [
            'locales' => ['en', 'bg'],
            'domains' => ['text', 'validators', 'messages'],
        ]);
    }
}
