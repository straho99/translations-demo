<?php

namespace App\Components;

use App\Entity\FavoriteProduct;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('favorite_products')]
class FavoriteProducts extends AbstractController
{
    use DefaultActionTrait;

    #[LiveProp(writable: true)]
    public array $selectedProducts = [];

    #[LiveProp(writable: true)]
    public array $selectedFavoriteProducts = [];

    #[LiveProp(writable: true)]
    public int $test = 1;

    public function __construct(private readonly EntityManagerInterface $manager)
    {
    }

    /** @return array<int, \App\Entity\Product> */
    public function products(): array
    {
        return $this->manager->getRepository(Product::class)->notAdded();
    }

    /** @return array<int, \App\Entity\FavoriteProduct> */
    public function favorites(): array
    {
        return $this->manager->getRepository(FavoriteProduct::class)->findAll();
    }

    #[LiveAction]
    public function addToFavorites(): void
    {
        foreach ($this->selectedProducts as $id) {
            $product = $this->manager->getRepository(Product::class)->find($id);
            $favorite = (new FavoriteProduct)->setProduct($product);
            $this->manager->persist($favorite);
        }

        $this->manager->flush();

        $this->selectedProducts = [];
    }

    #[LiveAction]
    public function removeFromFavorites(): void
    {
        foreach ($this->selectedFavoriteProducts as $id) {
            $favorite = $this->manager->getRepository(FavoriteProduct::class)->find($id);
            $this->manager->remove($favorite);
        }

        $this->manager->flush();

        $this->selectedFavoriteProducts = [];
    }

    #[LiveAction]
    public function selectAll(): void
    {
        $this->selectedProducts = array_map(fn(Product $product) => $product->getId(), $this->products());
    }

    #[LiveAction]
    public function removeSelected(): void
    {
        $this->selectedProducts = [];
    }

    #[LiveAction]
    public function selectAllFavorites(): void
    {
        $this->selectedFavoriteProducts = array_map(fn(FavoriteProduct $favoriteProduct) => $favoriteProduct->getId(), $this->favorites());
    }

    #[LiveAction]
    public function removeSelectedFavorites(): void
    {
        $this->selectedFavoriteProducts = [];
    }

    #[LiveAction]
    public function changeTest(#[LiveArg] int $value): void
    {
        $this->test = $value;
    }
}