<?php

namespace App\Components;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('translation')]
class Translation extends AbstractController
{
    use DefaultActionTrait;

    #[LiveProp]
    public int $translationId;

    #[LiveProp]
    public string $key;

    #[LiveProp(writable: true)]
    public ?string $text = null;

    public bool $editing = false;

    #[LiveAction]
    public function update(EntityManagerInterface $manager): void
    {
        $translation = $manager->getRepository(\App\Entity\Translation::class)->find($this->translationId);
        $translation->setTranslationValue($this->text);
        $manager->flush();

        $this->editing = false;
    }

    #[LiveAction]
    public function setEditing(#[LiveArg] bool $editing): void
    {
        $this->editing = $editing;
    }
}