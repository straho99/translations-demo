<?php

namespace App\Components;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent('translations')]
class Translations
{
    use DefaultActionTrait;

    #[LiveProp]
    public array $locales;

    #[LiveProp]
    public array $domains = [];

    #[LiveProp(writable: true)]
    public ?string $selectedLocale = null;

    #[LiveProp(writable: true)]
    public ?string $selectedDomain = null;

    #[LiveProp(writable: true)]
    public ?string $key = null;

    #[LiveProp(writable: true)]
    public ?string $text = null;

    #[LiveProp(writable: true)]
    public ?string $defaultLocale = 'en';

    public function __construct(private readonly EntityManagerInterface $manager)
    {
    }

    public function getTranslations(): array
    {
        return $this->manager->getRepository(\App\Entity\Translation::class)->filter(
            $this->selectedLocale,
            $this->selectedDomain,
            $this->key,
            $this->text
        );
    }

    #[LiveAction]
    public function clear(): void
    {
        $this->selectedLocale = $this->locales[0];
        $this->selectedDomain = $this->domains[0];
        $this->key = '';
        $this->text = '';
    }
}