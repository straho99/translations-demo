import { Controller } from '@hotwired/stimulus';
import { getComponent } from '@symfony/ux-live-component';

export default class extends Controller {
    async initialize() {
        this.component = await getComponent(this.element);
    }

    toggleTest() {
        // this.component.action('changeTest', {value: 3}); // This works

        // this.component.test = 3; // This does not work

        this.component.set('test', 3); // This does not work as well
    }
}
